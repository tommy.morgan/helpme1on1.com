(function($) {
  const refreshTimeoutInMS = 5000;

  let isPaused = false;
  let $question;
  let $pauseButton;
  let questionData;
  let intervalHandler;

  /**
   * Changes the question to a new random question.
   */
  function changeQuestion() {
    const arrayOfKeysAvailable = Object.keys(questionData);
    const questionLength = arrayOfKeysAvailable.length;
    let currentQuestion = 'We\'re out of questions!';

    if (questionLength !== 0) {
      const randomNumber = Math.floor(Math.random() * questionLength);
      const randomAvailableKey = arrayOfKeysAvailable[randomNumber];
      currentQuestion = questionData[randomAvailableKey];
      delete questionData[randomAvailableKey];
    } else {
      clearInterval(intervalHandler);
    }

    $question.html(currentQuestion);
  }

  /**
   * Toggles play/paused state for the page.
   * @returns {undefined}
   */
  function stateHandler() {
    isPaused = !isPaused;
    $pauseButton.html(isPaused ? 'Play' : 'Paused');
  }

  /**
   * Normalizes the questions into an indexed object so we can find and remove them with ease later on.
   * @param {Array<Object>} questionPayload  The payload from the questions API.
   * @returns {*} An object keyed by array index, with the question as the value.
   */
  function normalizeQuestions(questionPayload) {
    /* eslint-disable no-param-reassign */
    return questionPayload.reduce((accumulator, currentValue, index) => {
      accumulator[index] = currentValue.question;
      return accumulator;
    }, {});
    /* eslint-enable no-param-reassign */
  }

  // Main initialization function happens at $document.ready()
  function init() {
    $pauseButton = $('#pause-button');
    $question = $('#question');

    // register click handlers
    $pauseButton.on('click', stateHandler);

    // Get Questions Data
    $.getJSON('data/questions.json', function(data) {
      questionData = normalizeQuestions(data);
      changeQuestion();
    });
  }

  // DOM onready handler
  $().ready(init);

  // interval to cycle through questions - NOTE: no reason to add complexity of pausing/tracking interval -- instead just use flag for now
  intervalHandler = setInterval(function() {
    if (!isPaused) {
      changeQuestion();
    }
  }, refreshTimeoutInMS);
})(jQuery);
